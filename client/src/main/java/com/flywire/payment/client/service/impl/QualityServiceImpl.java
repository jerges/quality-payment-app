package com.flywire.payment.client.service.impl;

import com.flywire.payment.client.dto.Booking;
import com.flywire.payment.client.dto.Quality;
import com.flywire.payment.client.dto.QualityInfo;
import com.flywire.payment.client.exception.AmountThresholdException;
import com.flywire.payment.client.exception.DuplicatedPaymentException;
import com.flywire.payment.client.exception.EmailValidationException;
import com.flywire.payment.client.exception.QualityException;
import com.flywire.payment.client.service.QualityService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class QualityServiceImpl implements QualityService {

  private static final long LIMIT_AMOUNT_THRESHOLD = 1000000L;

  @Override
  public List<QualityInfo> qualityCheck(final List<Booking> bookings) {

    final List<QualityInfo> qualitiesInfo = new ArrayList<>();
    bookings.forEach(booking -> {
      final QualityInfo qualityInfo = QualityInfo.builder()
          .amount(booking.getAmount())
          .reference(booking.getReference())
          .amountReceive(booking.getAmountReceived())
          .currency(booking.getCurrencyFrom())
          .build();
      try {
        qualitiesInfo.add(qualityInfo.toBuilder().qualityCheck(this.qualityValidation(booking, bookings)).build());
      } catch (final QualityException e) {
        qualitiesInfo.add(qualityInfo.toBuilder().qualityCheck(e.getMessage()).build());
      }
    });
    return qualitiesInfo;
  }

  private String qualityValidation(final Booking booking, final List<Booking> bookings) {

    this.isValidEmail(booking.getEmail());
    this.isDuplicatedPayment(booking.getSenderFullName(), bookings);
    this.isAmountThreshold(booking.getAmount());
    return Quality.OK.getValue();

  }

  protected Quality isValidEmail(final String email) {
    if (!EmailValidator.getInstance().isValid(email)) {
      log.warn("The payment has an invalid email.");
      throw new EmailValidationException();
    }
    return Quality.OK;
  }

  protected Quality isDuplicatedPayment(final String senderFullName, final List<Booking> bookings) {
    if (bookings.stream()
        .filter(booking -> booking.getSenderFullName().equalsIgnoreCase(senderFullName))
        .count() > 1) {
      log.warn("The user that booked the payment has already a payment in the system");
      throw new DuplicatedPaymentException();
    }
    return Quality.OK;
  }

  protected Quality isAmountThreshold(final BigDecimal amountReceived) {
    if (amountReceived.compareTo(BigDecimal.valueOf(LIMIT_AMOUNT_THRESHOLD)) > 0) {
      log.warn("The amount of the payment is bigger than 1.000.000$");
      throw new AmountThresholdException();
    }
    return Quality.OK;

  }


}
