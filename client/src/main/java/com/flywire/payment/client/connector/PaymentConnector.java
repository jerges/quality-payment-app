package com.flywire.payment.client.connector;


import com.flywire.payment.client.dto.BookingResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(url = "http://localhost:9292/", name = "payment-booking")
public interface PaymentConnector {

  @GetMapping(value = "/api/bookings")
  BookingResult getPaymentBookings();

}
