package com.flywire.payment.client.exception;

import com.flywire.payment.client.dto.Quality;

public class AmountThresholdException extends QualityException {

  public AmountThresholdException() {
    super(Quality.AMOUNT_THRESHOLD.getValue());
  }
}
