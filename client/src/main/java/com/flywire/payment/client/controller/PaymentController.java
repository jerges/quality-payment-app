package com.flywire.payment.client.controller;

import com.flywire.payment.client.dto.QualityInfo;
import java.util.List;

public interface PaymentController {


  List<QualityInfo> getQualitiesPayment();

}
