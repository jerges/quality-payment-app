package com.flywire.payment.client.dto;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public final class Fee {

  private final BigDecimal percentage;
  private final FeeRuleRange feeRuleRange;
  private final CurrencyEnum currency;

}
