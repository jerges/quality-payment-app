package com.flywire.payment.client.dto;

import java.beans.ConstructorProperties;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public final class Booking {

  private final String reference;
  private final BigDecimal amount;
  private final BigDecimal amountReceived;
  private final String countryFrom;
  private final String senderFullName;
  private final String senderAddress;
  private final String school;
  private final String currencyFrom;
  private final Long studentId;
  private final String email;

  @ConstructorProperties({"reference", "amount", "amount_received", "country_from", "sender_fullName",
      "sender_address", "school", "currency_from", "student_id", "email"})
  public Booking(final String reference, final BigDecimal amount, final BigDecimal amountReceived,
      final String countryFrom,
      final String senderFullName, final String senderAddress, final String school, final String currencyFrom,
      final Long studentId, final String email) {
    this.reference = reference;
    this.amount = amount;
    this.amountReceived = amountReceived;
    this.countryFrom = countryFrom;
    this.senderFullName = senderFullName;
    this.senderAddress = senderAddress;
    this.school = school;
    this.currencyFrom = currencyFrom;
    this.studentId = studentId;
    this.email = email;
  }
}
