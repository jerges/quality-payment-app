package com.flywire.payment.client.dto;

import lombok.Getter;


public enum Quality {
  INVALID_EMAIL("InvalidEmail"),
  DUPLICATED_PAYMENT("DuplicatedPayment"),
  AMOUNT_THRESHOLD("AmountThreshold"),
  OK("ok");

  @Getter
  private final String value;

  Quality(final String value) {
    this.value = value;
  }
}
