package com.flywire.payment.client.service.impl;

import com.flywire.payment.client.connector.PaymentConnector;
import com.flywire.payment.client.dto.CurrencyEnum;
import com.flywire.payment.client.dto.QualityInfo;
import com.flywire.payment.client.resolver.FeeResolver;
import com.flywire.payment.client.service.PaymentService;
import com.flywire.payment.client.service.QualityService;
import feign.FeignException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PaymentServiceImpl implements PaymentService {

  private final FeeResolver feeResolver;
  private final QualityService qualityService;
  private final PaymentConnector connector;

  public PaymentServiceImpl(final FeeResolver feeResolver, final QualityService qualityService,
      final PaymentConnector connector) {
    this.feeResolver = feeResolver;
    this.qualityService = qualityService;
    this.connector = connector;
  }

  @Override
  public List<QualityInfo> paymentReview() {
    try {
     final var qualitiesInfo = this.qualityService.qualityCheck(
          this.connector.getPaymentBookings().getBookings());
      return qualitiesInfo
          .stream()
          .map(qualityInfo -> qualityInfo.toBuilder()
              .overPayment(this.isOverPayment(qualityInfo.getAmount(), qualityInfo.getAmountReceive()))
              .underpayment(this.isUnderPayment(qualityInfo.getAmount(), qualityInfo.getAmountReceive()))
              .amountFees(
                  this.amountFeeCalculator(qualityInfo.getAmount(), CurrencyEnum.value(qualityInfo.getCurrency())))
              .build()).collect(Collectors.toList());
    } catch (final FeignException e) {
      log.error(e.getMessage());
      return new ArrayList<>();
    }
  }


  protected BigDecimal amountFeeCalculator(final BigDecimal amount, final CurrencyEnum currency) {
    final var feePercentage = this.feeResolver.resolve(amount, currency);
    log.info("fee to apply:{} for amount: {}", feePercentage, amount);
    return amount.multiply(feePercentage).divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_EVEN)
        .add(amount);
  }

  protected boolean isUnderPayment(final BigDecimal amount, final BigDecimal amountReceive) {
    return amount.compareTo(amountReceive) > 0;

  }

  protected boolean isOverPayment(final BigDecimal amount, final BigDecimal amountReceive) {
    return amountReceive.compareTo(amount) > 0;
  }
}
