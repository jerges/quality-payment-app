package com.flywire.payment.client.dto;

import java.beans.ConstructorProperties;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class QualityInfo {

  String reference;
  @NonNull
  BigDecimal amount;
  BigDecimal amountFees;
  BigDecimal amountReceive;
  String qualityCheck;
  Boolean overPayment;
  Boolean underpayment;
  String currency;

  @ConstructorProperties({"reference", "amount", "amountFees", "amountReceive", "qualityCheck",
      "overPayment", "underpayment", "currency"})
  public QualityInfo(final String reference, @NonNull final BigDecimal amount, final BigDecimal amountFees,
      final BigDecimal amountReceive, final String qualityCheck, final Boolean overPayment, final Boolean underpayment,
      final String currency) {
    this.reference = reference;
    this.amount = amount;
    this.amountFees = amountFees;
    this.amountReceive = amountReceive;
    this.qualityCheck = qualityCheck;
    this.overPayment = overPayment;
    this.underpayment = underpayment;
    this.currency = currency;
  }
}
