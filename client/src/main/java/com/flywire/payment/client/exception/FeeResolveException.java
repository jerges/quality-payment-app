package com.flywire.payment.client.exception;

public class FeeResolveException extends RuntimeException {

  public FeeResolveException(final String message) {
    super(message);
  }

}
