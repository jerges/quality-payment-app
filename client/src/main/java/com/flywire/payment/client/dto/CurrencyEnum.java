package com.flywire.payment.client.dto;

public enum CurrencyEnum {
  EUR, ANY;

  public static CurrencyEnum value(final String value) {
    try {
      return CurrencyEnum.valueOf(value);
    } catch (final IllegalArgumentException e) {
      return CurrencyEnum.ANY;
    }
  }

}
