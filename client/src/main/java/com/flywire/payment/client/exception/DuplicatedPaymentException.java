package com.flywire.payment.client.exception;

import com.flywire.payment.client.dto.Quality;

public class DuplicatedPaymentException extends QualityException {

  public DuplicatedPaymentException() {
    super(Quality.DUPLICATED_PAYMENT.getValue());
  }
}
