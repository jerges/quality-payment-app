package com.flywire.payment.client.service;

import com.flywire.payment.client.dto.QualityInfo;
import java.util.List;

public interface PaymentService {

  List<QualityInfo> paymentReview();

}
