package com.flywire.payment.client.resolver;

import com.flywire.payment.client.dto.CurrencyEnum;
import java.math.BigDecimal;

public interface FeeResolver {

  BigDecimal resolve(BigDecimal amount, CurrencyEnum currency);

}
