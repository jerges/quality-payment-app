package com.flywire.payment.client.dto;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public final class FeeRuleRange {

  private final BigDecimal from;
  private final BigDecimal to;

}
