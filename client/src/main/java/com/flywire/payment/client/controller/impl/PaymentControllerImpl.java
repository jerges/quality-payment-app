package com.flywire.payment.client.controller.impl;

import com.flywire.payment.client.controller.PaymentController;
import com.flywire.payment.client.dto.QualityInfo;
import com.flywire.payment.client.service.PaymentService;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/payment")
public class PaymentControllerImpl implements PaymentController {

  private final PaymentService paymentService;

  public PaymentControllerImpl(final PaymentService paymentService) {
    this.paymentService = paymentService;
  }

  @Override
  @GetMapping
  public List<QualityInfo> getQualitiesPayment() {
    return this.paymentService.paymentReview();
  }
}
