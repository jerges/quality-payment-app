package com.flywire.payment.client.resolver.impl;

import com.flywire.payment.client.dto.CurrencyEnum;
import com.flywire.payment.client.dto.Fee;
import com.flywire.payment.client.dto.FeeRuleRange;
import com.flywire.payment.client.exception.FeeResolveException;
import com.flywire.payment.client.resolver.FeeResolver;
import java.math.BigDecimal;
import java.util.Set;
import org.springframework.stereotype.Component;

@Component
public class FeeResolverImpl implements FeeResolver {


  private final Set<Fee> feeRules;

  public FeeResolverImpl(final Set<Fee> feeItems) {
    this.feeRules = feeItems;
  }


  @Override
  public BigDecimal resolve(final BigDecimal amount, final CurrencyEnum currency) {
    return this.feeRules.stream()
        .filter(fee -> this.calculateFeePercentage(amount, fee.getFeeRuleRange()) && this.appliesCurrency(currency, fee))
        .map(Fee::getPercentage)
        .findFirst()
        .orElseThrow(() -> new FeeResolveException("The fee could not be calculated"));
  }


  protected boolean calculateFeePercentage(final BigDecimal amount, final FeeRuleRange feeRuleRange) {
    return (amount.compareTo(feeRuleRange.getFrom()) == 0 || amount.compareTo(feeRuleRange.getFrom()) > 0) &&
        (amount.compareTo(feeRuleRange.getTo()) < 0);
  }

  protected boolean appliesCurrency(final CurrencyEnum currency, final Fee fee) {
    return currency.equals(fee.getCurrency());
  }


}
