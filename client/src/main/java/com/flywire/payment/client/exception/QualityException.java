package com.flywire.payment.client.exception;

public class QualityException extends RuntimeException {

  public QualityException(final String message) {
    super(message);
  }

}
