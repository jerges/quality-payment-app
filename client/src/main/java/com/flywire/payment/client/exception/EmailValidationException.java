package com.flywire.payment.client.exception;

import com.flywire.payment.client.dto.Quality;

public class EmailValidationException extends QualityException {

  public EmailValidationException() {
    super(Quality.INVALID_EMAIL.getValue());
  }
}
