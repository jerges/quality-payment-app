package com.flywire.payment.client.config;

import com.flywire.payment.client.dto.CurrencyEnum;
import com.flywire.payment.client.dto.Fee;
import com.flywire.payment.client.dto.FeeRuleRange;
import java.math.BigDecimal;
import java.util.LinkedHashSet;
import java.util.Set;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeeRuleConfiguration {


  @Bean
  public Set<Fee> feeRules() {
    final Set<Fee> feeItems = new LinkedHashSet<>();
    feeItems.add(new Fee(BigDecimal.valueOf(5), new FeeRuleRange(BigDecimal.ZERO, BigDecimal.valueOf(1000)),
        CurrencyEnum.ANY));
    feeItems.add(new Fee(BigDecimal.valueOf(5.5), new FeeRuleRange(BigDecimal.ZERO, BigDecimal.valueOf(1000)),
        CurrencyEnum.EUR));
    feeItems.add(new Fee(BigDecimal.valueOf(4), new FeeRuleRange(BigDecimal.valueOf(1000), BigDecimal.valueOf(5000))
        , CurrencyEnum.EUR));
    feeItems.add(new Fee(BigDecimal.valueOf(3.5), new FeeRuleRange(BigDecimal.valueOf(5000), BigDecimal.valueOf(10000))
        , CurrencyEnum.EUR));
    feeItems.add(new Fee(BigDecimal.valueOf(3), new FeeRuleRange(BigDecimal.valueOf(1000), BigDecimal.valueOf(10000))
        , CurrencyEnum.ANY));
    feeItems.add(new Fee(BigDecimal.valueOf(2.5), new FeeRuleRange(BigDecimal.valueOf(10000),
        BigDecimal.valueOf(Double.MAX_VALUE)), CurrencyEnum.EUR));
    feeItems.add(new Fee(BigDecimal.valueOf(2), new FeeRuleRange(BigDecimal.valueOf(10000),
        BigDecimal.valueOf(Double.MAX_VALUE)), CurrencyEnum.ANY));
    return feeItems;
  }


}
