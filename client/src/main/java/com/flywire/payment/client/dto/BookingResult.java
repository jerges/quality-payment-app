package com.flywire.payment.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.beans.ConstructorProperties;
import java.util.List;
import lombok.Data;

@Data
public final class BookingResult {

  @JsonProperty("bookings")
  private final List<Booking> bookings;

  @ConstructorProperties("bookings")
  public BookingResult(final List<Booking> bookings) {
    this.bookings = bookings;
  }
}
