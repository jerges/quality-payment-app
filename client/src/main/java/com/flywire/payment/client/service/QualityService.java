package com.flywire.payment.client.service;

import com.flywire.payment.client.dto.Booking;
import com.flywire.payment.client.dto.QualityInfo;
import java.util.List;
import java.util.Set;

public interface QualityService {


  List<QualityInfo> qualityCheck(List<Booking> bookings);


}
