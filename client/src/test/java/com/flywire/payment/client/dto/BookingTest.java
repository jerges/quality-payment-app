package com.flywire.payment.client.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.flywire.payment.client.objectmother.BookingMother;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class BookingTest extends AbstractTestDto<Booking, BookingTest> {

  public BookingTest() {
    super(Booking.class, BookingTest.class);
  }

  private final JsonTester tester = new JsonTester();

  private static final String JSON_RESPONSE_EXAMPLE = "{\n"
      + "  \"reference\": \"107036651\",\n"
      + "  \"amount\": 4354517,\n"
      + "  \"amount_received\": 4354517,\n"
      + "  \"country_from\": \"Coruscant\",\n"
      + "  \"sender_full_name\": \"Borvo the Hutt\",\n"
      + "  \"sender_address\": \"581 Weissnat Center\",\n"
      + "  \"school\": \"Goldner University\",\n"
      + "  \"currency_from\": \"CAD\",\n"
      + "  \"student_id\": 828040,\n"
      + "  \"email\": \"providenci.cronin@schoen.info\"\n"
      + "}\n";

  public static final Booking BOOKING = BookingMother.withAllProperties();

  @Test
  public void shouldSerializeProperty() throws IOException {
    assertThat(this.tester.write(BOOKING)).isEqualToJson(JSON_RESPONSE_EXAMPLE);
  }
}
