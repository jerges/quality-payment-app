package com.flywire.payment.client.connector.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.flywire.payment.client.controller.impl.PaymentControllerImpl;
import com.flywire.payment.client.service.PaymentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PaymentConnectorImplTest {

  @InjectMocks
  private PaymentControllerImpl controller;

  @Mock
  private PaymentService paymentService;

  @Test
  public void getBookings() {
    controller.getQualitiesPayment();
    verify(paymentService, times(1)).paymentReview();
  }
}
