package com.flywire.payment.client.service.impl;

import static org.junit.Assert.assertEquals;

import com.flywire.payment.client.dto.Booking;
import com.flywire.payment.client.dto.Quality;
import com.flywire.payment.client.exception.AmountThresholdException;
import com.flywire.payment.client.exception.DuplicatedPaymentException;
import com.flywire.payment.client.exception.EmailValidationException;
import com.flywire.payment.client.objectmother.BookingMother;
import com.flywire.payment.client.objectmother.QualityInfoMother;
import com.flywire.payment.client.util.JsonReader;
import java.math.BigDecimal;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class QualityServiceImplTest extends JsonReader {

  @InjectMocks
  private QualityServiceImpl qualityService;


  private BookingMother bookingMother;

  private QualityInfoMother qualityInfoMotherExpected;

  @Before
  public void setUp() {
    this.bookingMother = this.loadObject("bookings.json", BookingMother.class);
    this.qualityInfoMotherExpected = this.loadObject("qualities-info-without-fee.json", QualityInfoMother.class);
  }

  @Test
  public void qualityCheckTest() {
    assertEquals(this.qualityInfoMotherExpected.getQualityInfo(),
        this.qualityService.qualityCheck(this.bookingMother.getBookings()));
  }


  @Test
  public void isValidEmailTest() {
    assertEquals(Quality.OK, this.qualityService.isValidEmail("providenci.cronin@schoen.info"));
  }

  @Test(expected = EmailValidationException.class)
  public void isInValidEmailTest() {
    this.qualityService.isValidEmail("providenci.cronin_schoen.info");
  }

  @Test(expected = DuplicatedPaymentException.class)
  public void isDuplicatedPaymentTest() {
    this.qualityService.isDuplicatedPayment("Han Solo", this.bookingMother.getBookings());
  }

  @Test
  public void isNotDuplicatedPaymentTest() {
    assertEquals(Quality.OK, this.qualityService.isDuplicatedPayment("Luke Skywalker",
        this.bookingMother.getBookings()));
  }

  @Test(expected = AmountThresholdException.class)
  public void isAmountThresholdTest() {
    final Booking booking = BookingMother.withAllProperties();
    this.qualityService.isAmountThreshold(booking.getAmountReceived());
  }

  @Test
  public void isNotAmountThresholdTest() {
    final Booking booking =
        BookingMother.withAllProperties().toBuilder().amountReceived(BigDecimal.valueOf(150000)).build();
    assertEquals(Quality.OK, this.qualityService.isAmountThreshold(booking.getAmountReceived()));
  }

}
