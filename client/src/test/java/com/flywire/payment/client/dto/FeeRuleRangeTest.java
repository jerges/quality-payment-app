package com.flywire.payment.client.dto;

public class FeeRuleRangeTest extends AbstractTestDto<FeeRuleRange, FeeRuleRangeTest> {

  public FeeRuleRangeTest() {
    super(FeeRuleRange.class, FeeRuleRangeTest.class);
  }

}
