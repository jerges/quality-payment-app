package com.flywire.payment.client.resolver.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flywire.payment.client.config.FeeRuleConfiguration;
import com.flywire.payment.client.dto.CurrencyEnum;
import com.flywire.payment.client.dto.FeeRuleRange;
import com.flywire.payment.client.exception.FeeResolveException;
import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FeeResolverImplTest {

  private FeeResolverImpl feeResolver;

  @Before
  public void setUp() {
    final FeeRuleConfiguration configuration = new FeeRuleConfiguration();
    this.feeResolver = new FeeResolverImpl(configuration.feeRules());
  }

  @Test
  public void resolveWhenIsNotEURCurrencyAndLess1000Test() {
    assertEquals(BigDecimal.valueOf(5), this.feeResolver.resolve(BigDecimal.TEN, CurrencyEnum.ANY));
  }

  @Test
  public void resolveWhenIsEURCurrencyAndLess1000Test() {
    assertEquals(BigDecimal.valueOf(5.5), this.feeResolver.resolve(BigDecimal.TEN, CurrencyEnum.EUR));
  }

  @Test
  public void resolveWhenIsEURCurrencyAndMoreThan1000Test() {
    assertEquals(BigDecimal.valueOf(4), this.feeResolver.resolve(BigDecimal.valueOf(1500), CurrencyEnum.EUR));
  }

  @Test
  public void resolveWhenIsEURCurrencyAndBetween5000_10000Test() {
    assertEquals(BigDecimal.valueOf(3.5), this.feeResolver.resolve(BigDecimal.valueOf(8000), CurrencyEnum.EUR));
  }

  @Test
  public void resolveWhenIsNotEURCurrencyAndMoreThan1000Test() {
    assertEquals(BigDecimal.valueOf(3), this.feeResolver.resolve(BigDecimal.valueOf(1500), CurrencyEnum.ANY));
  }

  @Test
  public void resolveWhenIsEURCurrencyAndAmountMaxTest() {
    assertEquals(BigDecimal.valueOf(2.5), this.feeResolver.resolve(BigDecimal.valueOf(15000), CurrencyEnum.EUR));
  }

  @Test
  public void resolveWhenIsNotEURCurrencyAndAmountMaxTest() {
    assertEquals(BigDecimal.valueOf(2), this.feeResolver.resolve(BigDecimal.valueOf(15000), CurrencyEnum.ANY));
  }


  @Test(expected = FeeResolveException.class)
  public void unResolvePercentageTest() {
    this.feeResolver.resolve(BigDecimal.valueOf(-1500), CurrencyEnum.ANY);
  }

  @Test
  public void calculateFeeFivePercentageTest() {
    assertTrue(this.feeResolver.calculateFeePercentage(BigDecimal.TEN, new FeeRuleRange(BigDecimal.ZERO,
        BigDecimal.valueOf(1000))));
  }

  @Test
  public void calculateFeeFivePercentageLimitTest() {
    assertFalse(
        this.feeResolver.calculateFeePercentage(BigDecimal.valueOf(1000), new FeeRuleRange(BigDecimal.ZERO,
            BigDecimal.valueOf(1000))));
  }

  @Test
  public void calculateFeeThreePercentageTest() {
    assertTrue(
        this.feeResolver.calculateFeePercentage(BigDecimal.valueOf(1000),
            new FeeRuleRange(BigDecimal.valueOf(1000), BigDecimal.valueOf(10000))));
  }

  @Test
  public void calculateFeeThreePercentageLimitTest() {
    assertFalse(
        this.feeResolver.calculateFeePercentage(BigDecimal.valueOf(10000),
            new FeeRuleRange(BigDecimal.valueOf(1000), BigDecimal.valueOf(10000))));
  }

  @Test
  public void calculateFeeTwoPercentageTest() {
    assertTrue(
        this.feeResolver.calculateFeePercentage(BigDecimal.valueOf(10000),
            new FeeRuleRange(BigDecimal.valueOf(10000), BigDecimal.valueOf(Double.MAX_VALUE))));
  }


}
