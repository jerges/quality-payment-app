package com.flywire.payment.client.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.flywire.payment.client.objectmother.QualityInfoMother;
import java.io.IOException;
import org.junit.Test;

public final class QualityInfoTest extends AbstractTestDto<QualityInfo, QualityInfoTest> {

  public QualityInfoTest() {
    super(QualityInfo.class, QualityInfoTest.class);
  }

  private static final String JSON_RESPONSE_EXAMPLE = "{\n"
      + "  \"reference\" : \"123456\",\n"
      + "  \"amount\" : 4354517,\n"
      + "  \"amount_fees\" : 0,\n"
      + "  \"amount_receive\" : 0,\n"
      + "  \"quality_check\" : \"ok\",\n"
      + "  \"over_payment\" : false,\n"
      + "  \"underpayment\" : false\n"
      + "}";


  private final JsonTester tester = new JsonTester();

  public static final QualityInfo QUALITY_INFO = QualityInfoMother.withAllProperties();

  @Test
  public void shouldDeserializeProperty() throws IOException {
    assertThat(this.tester.write(QUALITY_INFO)).isEqualToJson(JSON_RESPONSE_EXAMPLE);
  }
}
