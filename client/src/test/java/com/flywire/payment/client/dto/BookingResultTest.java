package com.flywire.payment.client.dto;

public class BookingResultTest extends AbstractTestDto<BookingResult, BookingResultTest> {

  public BookingResultTest() {
    super(BookingResult.class, BookingResultTest.class);
  }
}
