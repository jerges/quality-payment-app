package com.flywire.payment.client.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class QualityTest {

  @Test
  public void getValue() {
    assertEquals("AmountThreshold", Quality.AMOUNT_THRESHOLD.getValue());
    assertEquals("DuplicatedPayment", Quality.DUPLICATED_PAYMENT.getValue());
    assertEquals("InvalidEmail", Quality.INVALID_EMAIL.getValue());
    assertEquals("ok", Quality.OK.getValue());
  }
}
