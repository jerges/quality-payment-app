package com.flywire.payment.client;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.flywire.payment.client.connector.PaymentConnector;
import com.flywire.payment.client.controller.PaymentController;
import com.flywire.payment.client.dto.BookingResult;
import com.flywire.payment.client.objectmother.BookingMother;
import com.flywire.payment.client.objectmother.QualityInfoMother;
import com.flywire.payment.client.resolver.FeeResolver;
import com.flywire.payment.client.service.PaymentService;
import com.flywire.payment.client.service.QualityService;
import com.flywire.payment.client.util.JsonReader;
import feign.FeignException;
import java.util.Collections;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ClientApplication.class})
public class ClientApplicationTests extends JsonReader {

  @Autowired
  private PaymentController controller;

  @Autowired
  private FeeResolver feeResolver;

  @Autowired
  private PaymentService paymentService;

  @Autowired
  private QualityService qualityService;

  @MockBean
  private PaymentConnector connector;

  private QualityInfoMother expected;
  private BookingResult bookingResult;

  @Before
  public void setUp() {
    final var bookingMother = this.loadObject("bookings.json", BookingMother.class);
    this.bookingResult = new BookingResult(bookingMother.getBookings());
    this.expected = this.loadObject("qualities-all-info.json", QualityInfoMother.class);
  }

  @Test
  public void getQualitiesInfo() {
    when(this.connector.getPaymentBookings()).thenReturn(this.bookingResult);
    assertEquals(this.expected.getQualityInfo(), this.controller.getQualitiesPayment());
  }

  @Test
  public void getQualitiesInfoError() {
    when(this.connector.getPaymentBookings()).thenThrow(FeignException.class);
    assertEquals(Collections.emptyList(), this.controller.getQualitiesPayment());
  }

}
