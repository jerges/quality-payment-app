package com.flywire.payment.client.objectmother;

import com.flywire.payment.client.dto.Quality;
import com.flywire.payment.client.dto.QualityInfo;
import java.math.BigDecimal;
import java.util.List;
import lombok.Data;

@Data
public final class QualityInfoMother {

  public static QualityInfo withAllProperties() {
    return QualityInfo
        .builder()
        .reference("123456")
        .amount(BigDecimal.valueOf(4354517))
        .amountFees(BigDecimal.ZERO)
        .amountReceive(BigDecimal.ZERO)
        .qualityCheck(Quality.OK.getValue())
        .overPayment(false)
        .underpayment(false)
        .build();
  }

  private List<QualityInfo> qualityInfo;

}
