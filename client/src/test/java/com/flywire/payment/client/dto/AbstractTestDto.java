package com.flywire.payment.client.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flywire.payment.client.config.JacksonConfiguration;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.core.ResolvableType;


@RunWith(MockitoJUnitRunner.class)
public abstract class AbstractTestDto<O, T> {

  private final Class<O> dto;
  private final Class<T> testClass;
  protected final ObjectMapper objectMapper = new JacksonConfiguration().createObjectMapper();

  protected AbstractTestDto(final Class<O> dto, final Class<T> testClass) {
    this.dto = dto;
    this.testClass = testClass;
  }

  @Test
  public void equalsVerifier() {
    EqualsVerifier.forClass(this.dto)
        .suppress(Warning.ANNOTATION)
        .verify();
  }

  public class JsonTester extends JacksonTester<O> {

    public JsonTester() {
      super(AbstractTestDto.this.testClass, ResolvableType.forClass(AbstractTestDto.this.dto),
          AbstractTestDto.this.objectMapper);
      JsonTester.initFields(AbstractTestDto.this.dto, new JacksonConfiguration().createObjectMapper());
    }
  }

}
