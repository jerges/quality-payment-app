package com.flywire.payment.client.util;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.flywire.payment.client.config.JacksonConfiguration;
import java.io.InputStream;

public class JsonReader {

  protected final ObjectMapper objectMapper = new JacksonConfiguration().createObjectMapper();

  protected <T> T loadObject(final String filename, final Class<T> theType) {
    String pkg = theType.getPackageName();
    pkg = pkg.replace(".", "/");
    final String file = "/" + pkg + "/" + filename;
    final InputStream jsonInput = this.getClass().getResourceAsStream(file);
    T result = null;
    try {
      result = this.objectMapper.readValue(jsonInput, theType);
    } catch (final Exception e) {
      e.printStackTrace();
    }
    return result;
  }

}
