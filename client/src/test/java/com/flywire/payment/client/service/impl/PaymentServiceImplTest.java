package com.flywire.payment.client.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.flywire.payment.client.connector.PaymentConnector;
import com.flywire.payment.client.dto.BookingResult;
import com.flywire.payment.client.dto.CurrencyEnum;
import com.flywire.payment.client.objectmother.BookingMother;
import com.flywire.payment.client.objectmother.QualityInfoMother;
import com.flywire.payment.client.resolver.FeeResolver;
import com.flywire.payment.client.service.QualityService;
import com.flywire.payment.client.util.JsonReader;
import feign.FeignException;
import java.math.BigDecimal;
import java.util.Collections;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class PaymentServiceImplTest extends JsonReader {

  @InjectMocks
  private PaymentServiceImpl paymentService;

  @Mock
  private FeeResolver feeResolver;

  @Mock
  private QualityService qualityService;

  @Mock
  private PaymentConnector connector;


  private BookingResult bookingResult;

  private QualityInfoMother qualityInfoMother;

  private QualityInfoMother qualityInfoMotherExpected;

  @Before
  public void setUp() {
    final var bookingMother = this.loadObject("bookings.json", BookingMother.class);
    this.qualityInfoMother = this.loadObject("qualities-info-without-fee.json", QualityInfoMother.class);
    this.qualityInfoMotherExpected = this.loadObject("qualities-all-info.json", QualityInfoMother.class);
    this.bookingResult = new BookingResult(bookingMother.getBookings());
    when(this.feeResolver.resolve(BigDecimal.valueOf(200), CurrencyEnum.EUR)).thenReturn(BigDecimal.valueOf(5.5));
    when(this.feeResolver.resolve(BigDecimal.valueOf(122321352), CurrencyEnum.ANY)).thenReturn(BigDecimal.valueOf(2));
    when(this.feeResolver.resolve(BigDecimal.valueOf(118673303), CurrencyEnum.EUR)).thenReturn(BigDecimal.valueOf(2.5));
    when(this.feeResolver.resolve(BigDecimal.valueOf(74245616), CurrencyEnum.ANY)).thenReturn(BigDecimal.valueOf(2));
    when(this.feeResolver.resolve(BigDecimal.valueOf(40667467), CurrencyEnum.EUR)).thenReturn(BigDecimal.valueOf(2.5));
    when(this.feeResolver.resolve(BigDecimal.valueOf(4354517), CurrencyEnum.ANY)).thenReturn(BigDecimal.valueOf(2));
    when(this.feeResolver.resolve(BigDecimal.valueOf(3450), CurrencyEnum.EUR)).thenReturn(BigDecimal.valueOf(4));
    when(this.feeResolver.resolve(BigDecimal.valueOf(70355288), CurrencyEnum.ANY)).thenReturn(BigDecimal.valueOf(2));
  }

  @Test
  public void amountFeeCalculatorTest() {
    when(this.feeResolver.resolve(any(), any())).thenReturn(BigDecimal.valueOf(5));
    assertEquals(BigDecimal.valueOf(10500, 2),
        this.paymentService.amountFeeCalculator(BigDecimal.valueOf(100), CurrencyEnum.ANY));
    assertEquals(BigDecimal.valueOf(21000, 2),
        this.paymentService.amountFeeCalculator(BigDecimal.valueOf(200), CurrencyEnum.ANY));
    assertEquals(BigDecimal.valueOf(246.75),
        this.paymentService.amountFeeCalculator(BigDecimal.valueOf(235), CurrencyEnum.ANY));
  }

  @Test
  public void paymentReviewTest() {
    when(this.connector.getPaymentBookings()).thenReturn(this.bookingResult);
    when(this.qualityService.qualityCheck(any())).thenReturn(this.qualityInfoMother.getQualityInfo());
    assertEquals(this.qualityInfoMotherExpected.getQualityInfo(), this.paymentService.paymentReview());
  }

  @Test
  public void paymentReviewConnectionErrorTest() {
    when(this.connector.getPaymentBookings()).thenThrow(FeignException.class);
    assertEquals(Collections.EMPTY_LIST, this.paymentService.paymentReview());
  }

  @Test
  public void isUnderPaymentTest() {
    assertTrue(this.paymentService.isUnderPayment(BigDecimal.TEN, BigDecimal.ONE));
  }

  @Test
  public void isOverPaymentTest() {
    assertTrue(this.paymentService.isOverPayment(BigDecimal.ONE, BigDecimal.TEN));
  }

  @Test
  public void isNotUnderPaymentAndNotOverPaymentTest() {
    assertFalse(this.paymentService.isUnderPayment(BigDecimal.TEN, BigDecimal.TEN));
    assertFalse(this.paymentService.isOverPayment(BigDecimal.TEN, BigDecimal.TEN));
  }
}
