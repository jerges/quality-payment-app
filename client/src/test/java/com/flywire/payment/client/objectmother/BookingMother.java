package com.flywire.payment.client.objectmother;

import com.flywire.payment.client.dto.Booking;
import java.math.BigDecimal;
import java.util.List;
import lombok.Data;

@Data
public final class BookingMother {

  public static Booking withAllProperties() {
    return Booking
        .builder()
        .amount(BigDecimal.valueOf(4354517))
        .amountReceived(BigDecimal.valueOf(4354517))
        .countryFrom("Coruscant")
        .currencyFrom("CAD")
        .email("providenci.cronin@schoen.info")
        .reference("107036651")
        .school("Goldner University")
        .senderAddress("581 Weissnat Center")
        .senderFullName("Borvo the Hutt")
        .studentId(828040L)
        .build();
  }

  private List<Booking> bookings;


}
