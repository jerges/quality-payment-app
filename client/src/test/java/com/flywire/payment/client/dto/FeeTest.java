package com.flywire.payment.client.dto;

public class FeeTest extends AbstractTestDto<Fee, FeeTest> {

  public FeeTest() {
    super(Fee.class, FeeTest.class);
  }
}
